# CarRentalSystem

Ce programme a été réalisé dans le cadre du projet de test logiciel de l'UV AI03.

Il simule un magasin de location de voitures à des clients. L'objectif était ensuite de tester ce programme à l'aide de l'outil de test logiciel Cucumber.

## Installation

Pour installer le programme, il suffit de l'importer en tant que projet Maven dans un IDE tel que, par exemple, IntelliJ. L'ensemble de la procédure se fera ainsi automatiquement.

L'exécution de tests pourra ensuite se faire directement via l'IDE, en exécutant le runner CucumberTestRunner (clic droit -> run) ou chaque fichier de feature un à un (clic droit -> run).