package fr.ghocleme.exception;

public class CarRegistrationException extends RuntimeException {

    public CarRegistrationException(String message) {
        super(message);
    }

    public CarRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
