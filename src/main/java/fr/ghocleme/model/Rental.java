package fr.ghocleme.model;

import java.util.Objects;

public class Rental {

    private final Customer customer;
    private final Car car;
    private int days;

    public Rental(Customer customer, Car car, int days) {
        this.customer = customer;
        this.car = car;
        this.days = days;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public Car getCar() {
        return this.car;
    }

    public int getDays() {
        return this.days;
    }

    public void addDays(int days) {
        this.days += days;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rental rental = (Rental) o;
        return days == rental.days && customer.equals(rental.customer) && car.equals(rental.car);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, car, days);
    }
}
