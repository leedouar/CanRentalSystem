package fr.ghocleme.model;

import fr.ghocleme.exception.CarRentalException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CarRentalSystem {

    private final CarManager manager;
    private final Map<Customer, Rental> rentals;

    public CarRentalSystem(CarManager manager) {
        this.manager = manager;
        this.rentals = new HashMap<>();
    }

    public Rental rentCar(Customer customer, Car car, int days) {

        if(this.hasRental(customer))
            throw new CarRentalException("Customer already has a rental.");

        if(!this.manager.hasCar(car))
            throw new CarRentalException("Car not found.");

        if(!this.isCarAvailable(car))
            throw new CarRentalException("Car not available.");

        if(days > 10)
            throw new CarRentalException("Car cannot be rent during more than 10 days.");

        Rental rental = new Rental(customer, car, days);

        this.rentals.put(customer, rental);

        return rental;
    }

    public void returnCar(Customer customer) {

        if(!this.hasRental(customer))
            throw new CarRentalException("Customer did not rent a car.");

        this.rentals.remove(customer);
    }

    public void prolongRental(Customer customer, int days) {

        if(!this.hasRental(customer))
            throw new CarRentalException("Customer did not rent a car.");

        Rental rental = this.rentals.get(customer);

        /*
        if(days + rental.getDays() > 10)
            throw new CarRentalException("Car cannot be rent during more than 10 days.");
        */

        rental.addDays(days);
    }

    public boolean isCarAvailable(Car car) {
        return this.rentals.values().stream().noneMatch(rent -> rent.getCar().equals(car));
    }

    public boolean hasRental(Customer customer) {
        return this.rentals.containsKey(customer);
    }

    public Rental getRental(Customer customer) {
        return this.rentals.getOrDefault(customer, null);
    }

    public Collection<Rental> getRentals() {
        return this.rentals.values();
    }

    public CarManager getCarManager() {
        return this.manager;
    }
}
