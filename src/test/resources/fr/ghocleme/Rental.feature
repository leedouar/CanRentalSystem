Feature: Rent a car

  As a user
  I want to rent and return a car
  So I can travel easily

  Background:
    Given the car rental system is running
    And there are available cars in the stock
    And a customer

  Scenario: Rent a car
    When The customer select a car to rent
    Then the car should not be available anymore
    And the rental should be added to the rentals list

  Scenario: Rent two cars
    Given the customer has a rental for 3 days
    When The customer select another car to rent
    Then then rental should not be allowed

  Scenario: Return a car
    Given the customer has a rental for 3 days
    When The customer returns the car
    Then the car should now be available
    And the rental should be removed from the rentals list

  Scenario Outline: Extend a rental
    Given the customer has a rental for <rent> days
    When he wants to extend it by <extension> days
    Then the rental should be extended to <result> days
    Examples:
      | rent | extension | result |
      | 3    | 4         | 7      |
      | 1    | 5         | 6      |
      | 7    | 3         | 10     |

  Scenario: Extend a rental for more than 10 days
    Given the customer has a rental for 3 days
    When he wants to extend it by 8 days
    Then the rental should not be extended